package com.aiproano;

import com.kumuluz.ee.discovery.annotations.DiscoverService;
import com.kumuluz.ee.discovery.enums.AccessType;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author aiproano.
 */
@Path("/gateway")
@RequestScoped
public class GatewayService {
    @Inject
    @DiscoverService(value = "persona-service", accessType = AccessType.DIRECT)
    private String personaServiceUrl;

    @Inject
    @DiscoverService(value = "direccion-service", accessType = AccessType.DIRECT)
    private String direccionServiceUrl;

    @GET
    @Path("/persona")
    @Produces(MediaType.APPLICATION_JSON)
    public String personaServiceUrl() {
        return personaServiceUrl;
    }

    @GET
    @Path("/direccion")
    @Produces(MediaType.APPLICATION_JSON)
    public String direccionServiceUrl() {
        return direccionServiceUrl;
    }


}
