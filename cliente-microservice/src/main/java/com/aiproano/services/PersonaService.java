package com.aiproano.services;

import com.aiproano.dto.Persona;

import javax.annotation.ManagedBean;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


/**
 * @author aiproano.
 */

@ManagedBean
@Path("/persona")
public interface PersonaService {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Persona> findAll();


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    Persona addPersona(Persona persona);

    @POST
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    Persona updatePersona(Persona persona);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    Persona findById(@PathParam("id") long id);

    @DELETE
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    boolean deletePersona(@PathParam("id") long id);
}

