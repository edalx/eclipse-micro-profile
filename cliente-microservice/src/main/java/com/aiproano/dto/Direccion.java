package com.aiproano.dto;


import java.io.Serializable;

/**
 * @author aiproano.
 */
public class Direccion implements Serializable {
    private long id;
    private String calle1;
    private String calle2;
    private String numero;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Direccion{" +
                "id=" + id +
                ", calle1='" + calle1 + '\'' +
                ", calle2='" + calle2 + '\'' +
                ", numero='" + numero + '\'' +
                '}';
    }

    public String getCalle1() {
        return calle1;
    }

    public void setCalle1(String calle1) {
        this.calle1 = calle1;
    }

    public String getCalle2() {
        return calle2;
    }

    public void setCalle2(String calle2) {
        this.calle2 = calle2;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
