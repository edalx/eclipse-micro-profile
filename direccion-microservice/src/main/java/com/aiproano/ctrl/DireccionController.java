package com.aiproano.ctrl;

import com.aiproano.dto.Direccion;
import com.aiproano.services.DireccionServiceImpl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author aiproano.
 */
@Path("/direccion")
@RequestScoped
public class DireccionController {
    @Inject
    private DireccionServiceImpl direccionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Direccion> findAll() {
        return direccionService.findAll();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Direccion addDireccion(Direccion direccion) {
        return direccionService.save(direccion);
    }

    @POST //Se usa POST dado que @PATCH no es reconocida
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Direccion updateDireccion(Direccion direccion) {
        return direccionService.update(direccion);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Direccion findById(@PathParam("id") long id) {
        return direccionService.findById(id);
    }

    @DELETE
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean deleteDireccion(@PathParam("id") long id) {
        return direccionService.delete(id);
    }

}

