package com.aiproano.services;

import com.aiproano.dao.DireccionDAOImpl;
import com.aiproano.dto.Direccion;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * @author aiproano.
 */
@ApplicationScoped
public class DireccionServiceImpl implements DireccionService {
    @Inject
    private DireccionDAOImpl direccionDAO;

    @Override
    public Direccion findById(long id) {
        return direccionDAO.findDireccionById(id);
    }

    @Override
    public Direccion save(Direccion direccion) {
        return direccionDAO.insertDireccion(direccion);
    }

    @Override
    public boolean delete(long direccionId) {
        return direccionDAO.deleteDireccion(direccionId);
    }

    @Override
    public List<Direccion> findAll() {
        return direccionDAO.getAllDireccion();
    }

    @Override
    public Direccion update(Direccion direccion) {
        return direccionDAO.updateDireccion(direccion);
    }
}
