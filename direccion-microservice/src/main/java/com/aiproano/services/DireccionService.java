package com.aiproano.services;

import com.aiproano.dto.Direccion;

import java.util.List;

/**
 * @author aiproano.
 */
public interface DireccionService {

    Direccion findById(long id);

    Direccion save(Direccion direccion);

    boolean delete(long direccionId);

    List<Direccion> findAll();

    Direccion update(Direccion direccion);

}