package com.aiproano.dao;

import com.aiproano.dto.Direccion;

import java.util.List;

/**
 * @author aiproano.
 */
public interface DireccionDAO {

    Direccion findDireccionById(long id);

    Direccion insertDireccion(Direccion direccion);

    List<Direccion> getAllDireccion();

    boolean deleteDireccion(long direccionId);

    Direccion updateDireccion(Direccion direccion);

}
