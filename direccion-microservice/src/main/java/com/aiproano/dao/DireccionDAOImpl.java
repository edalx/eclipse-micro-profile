package com.aiproano.dao;

import com.aiproano.database.ConnectionFactory;
import com.aiproano.dto.Direccion;
import com.aiproano.services.ConsumeConfigApi;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aiproano.
 */
@ApplicationScoped
public class DireccionDAOImpl implements DireccionDAO {
    @Inject
    private ConnectionFactory connectionFactory;
    @Inject
    private ConsumeConfigApi consumeConfigApi;
    private final String POSTGRES_SCHEMA_KEY = "postgres-url";
    private String postgresSchema;

    @PostConstruct
    public void init() {
        this.postgresSchema = consumeConfigApi.getProperty(POSTGRES_SCHEMA_KEY);
    }

    @Override
    public Direccion findDireccionById(long idDireccion) {
        try {
            Connection connection = connectionFactory.getConnection();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + postgresSchema + "direccion WHERE id=" + idDireccion);
            connection.close(); //Cierra la conexion
            if (rs.next()) {
                return extractDireccionFromResultSet(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Direccion insertDireccion(Direccion direccion) {
        try {
            Connection connection = connectionFactory.getConnection();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + postgresSchema + "direccion (calle_1, calle_2, numero) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, direccion.getCalle1());
            ps.setString(2, direccion.getCalle2());
            ps.setString(3, direccion.getNumero());

            int affectedRows = ps.executeUpdate();
            connection.close(); //Cierra la conexion

            if (affectedRows == 0) {
                throw new SQLException("No se ha podido crear la dirección");
            }

            ResultSet generatedKeys = ps.getGeneratedKeys();

            if (generatedKeys.next()) {
                direccion.setId(generatedKeys.getLong(1));
                return direccion;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Direccion> getAllDireccion() {
        List<Direccion> direccions = new ArrayList<>();
        try {
            Connection connection = connectionFactory.getConnection();
            if (connection != null) {
                System.out.println("Conectado a la base de datos");
            } else {
                System.out.println("No se pudo conectar a la base de datos");
            }

            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + postgresSchema + "direccion");
            connection.close(); //Cierra la conexion

            while (rs.next()) {
                direccions.add(extractDireccionFromResultSet(rs));
            }
            return direccions;

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteDireccion(long direccionId) {
        try {
            Connection connection = connectionFactory.getConnection();
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM " + postgresSchema + "direccion WHERE id=" + direccionId);
            connection.close(); //Cierra la conexion

            if (i == 1) {
                return true;
            } else {
                throw new SQLException("No se ha podido eliminar la dirección");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public Direccion updateDireccion(Direccion direccion) {
        try {
            Connection connection = connectionFactory.getConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE " + postgresSchema + "direccion set calle_1=?, calle_2=?, numero=? where id=?");
            ps.setString(1, direccion.getCalle1());
            ps.setString(2, direccion.getCalle2());
            ps.setString(3, direccion.getNumero());
            ps.setLong(4, direccion.getId());

            int affectedRows = ps.executeUpdate();
            connection.close(); //Cierra la conexion

            if (affectedRows == 0) {
                throw new SQLException("No se ha podido actualizar la dirección");
            } else {
                return findDireccionById(direccion.getId());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Direccion extractDireccionFromResultSet(ResultSet rs) {
        try {
            Direccion direccion = new Direccion();
            direccion.setId(rs.getInt("id"));
            direccion.setCalle1(rs.getString("calle_1"));
            direccion.setCalle2(rs.getString("calle_2"));
            direccion.setNumero(rs.getString("numero"));
            return direccion;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
