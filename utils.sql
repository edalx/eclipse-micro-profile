CREATE SCHEMA distribuida_db;

USE distribuida_db;

CREATE TABLE Direccion
(
    id serial PRIMARY KEY,
    calle_1 VARCHAR(100) NOT NULL,
    calle_2 varchar(100) NOT NULL,
    numero varchar(100) NOT NULL
);

CREATE TABLE Persona
(
    id serial PRIMARY KEY,
    identificacion varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    fecha_nacimiento date NOT NULL,
    id_direccion int NOT NULL
);

INSERT INTO postgres.distribuida_db.direccion (id, calle_1, calle_2, numero) VALUES (1 , 'Camilo Orejuela', 'S55G', 'Oe8-50');
INSERT INTO postgres.distribuida_db.direccion (calle_1, calle_2, numero) VALUES ('Av. Patria', 'Av. 12 de Octubre', 'Oe8-50');
INSERT INTO distribuida_db.Persona (id, identificacion, nombre, fecha_nacimiento, id_direccion) VALUES (null , '1723510291', 'Alexander Cuji', '1993-09-10', 1);
INSERT INTO distribuida_db.Persona (id, identificacion, nombre, fecha_nacimiento, id_direccion) VALUES (null , '1600235244', 'Pedro Zambrano', '1998-09-11', 2);
