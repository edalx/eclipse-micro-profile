package com.aiproano;

import org.eclipse.microprofile.config.Config;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author aiproano.
 */
@Path("/config")
@RequestScoped
public class ConfigRest {
    @Inject
    private Config config;

    @GET
    @Path("/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public String configValue(@PathParam("key") String key) {
        String value = config.getOptionalValue(key, String.class).orElse("");
        return value;
    }
}
