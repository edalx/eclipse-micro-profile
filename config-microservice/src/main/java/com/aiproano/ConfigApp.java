package com.aiproano;


import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author aiproano.
 */
@ApplicationPath("/")
@ApplicationScoped
public class ConfigApp extends Application {
}
