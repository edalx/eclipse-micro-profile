package com.aiproano.database;

import com.aiproano.services.ConsumeConfigApi;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author aiproano.
 */

@ApplicationScoped
public class ConnectionFactory {
    private final String POSTGRES_URI_KEY = "postgres-uri";
    private final String POSTGRES_USER_KEY = "postgres-user";
    private final String POSTGRES_PASS_KEY = "postgres-pass";

    private String postgresUri;
    private String postgresUser;
    private String postgresPass;
    @Inject
    private ConsumeConfigApi consumeConfigApi;

    @PostConstruct
    public void init() {
        try {
            //Obtiene la propiedad del config server
            this.postgresUri = consumeConfigApi.getProperty(POSTGRES_URI_KEY);
            this.postgresUser = consumeConfigApi.getProperty(POSTGRES_USER_KEY);
            this.postgresPass = consumeConfigApi.getProperty(POSTGRES_PASS_KEY);

            System.out.println("postgresUri=" + postgresUri);
            System.out.println("postgresUser=" + postgresUser);
            System.out.println("postgresPass=" + postgresPass);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(postgresUri, postgresUser, postgresPass);
        } catch (SQLException ex) {
            throw new RuntimeException("Error connecting to the database", ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
