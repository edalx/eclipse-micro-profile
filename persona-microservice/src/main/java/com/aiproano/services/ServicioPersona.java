package com.aiproano.services;

import com.aiproano.dto.Persona;

import java.util.List;

/**
 * @author aiproano.
 */
public interface ServicioPersona {

    List<Persona> list();

    Persona save(Persona persona);

    boolean delete(long idPersona);

    Persona findById(long idPersona);

    Persona update(Persona persona);


}
