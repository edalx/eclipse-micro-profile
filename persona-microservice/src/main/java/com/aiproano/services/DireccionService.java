package com.aiproano.services;

import com.aiproano.dto.Direccion;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import java.util.List;

/**
 * @author aiproano.
 */
@RegisterRestClient
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
@Path("/direccion")
public interface DireccionService {
    @GET
    List<Direccion> findAll();

    @POST
    Direccion addDireccion(Direccion direccion);

    @POST
    @Path("/update")
    Direccion updateDireccion(Direccion direccion);

    @GET
    @Path("/{id}")
    Direccion findDireccionById(@PathParam("id") long id);

    @DELETE
    @Path("/delete/{id}")
    boolean deleteDireccion(@PathParam("id") long id);
}
