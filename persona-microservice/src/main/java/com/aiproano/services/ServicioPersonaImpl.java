package com.aiproano.services;

import com.aiproano.dao.PersonaDAOImpl;
import com.aiproano.dto.Persona;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * @author aiproano.
 */
@RequestScoped
public class ServicioPersonaImpl implements ServicioPersona {
    @Inject
    private PersonaDAOImpl personaDAO;

    @Override
    public List<Persona> list() {
        return personaDAO.getAllPersona();
    }

    @Override
    public Persona save(Persona persona) {
        return personaDAO.insertPersona(persona);
    }

    @Override
    public boolean delete(long idPersona) {
        return personaDAO.deletePersona(idPersona);
    }

    @Override
    public Persona findById(long idPersona) {
        return personaDAO.findPersonaById(idPersona);
    }

    @Override
    public Persona update(Persona persona) {
        return personaDAO.updatePersona(persona);
    }
}
