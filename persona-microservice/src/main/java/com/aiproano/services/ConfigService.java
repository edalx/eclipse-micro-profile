package com.aiproano.services;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * @author aiproano.
 */
@RegisterRestClient
@ApplicationScoped
@Path("/config")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ConfigService {

    @GET
    @Path("/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    String configValue(@PathParam("key") String key);
}
