package com.aiproano.services;

import com.aiproano.dto.Direccion;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;

/**
 * @author aiproano.
 */
@ApplicationScoped
public class ConsumeDireccionApi {
    @RestClient
    private DireccionService direccionService;

    @Inject
    private ConsumeConfigApi consumeConfigApi;

    private final String URL_KEY_DIRECCION_MICROSERVICE = "url-direccion-microservice"; //key url microservicio direccion

    private String urlValueDireccionMicroservice;

    @PostConstruct
    public void init() {
        try {
            //Obtiene la propiedad del config server
            this.urlValueDireccionMicroservice = consumeConfigApi.getProperty(URL_KEY_DIRECCION_MICROSERVICE);
            System.out.println("Dirección service: "+urlValueDireccionMicroservice);
            this.direccionService = RestClientBuilder.newBuilder().baseUri(new URI(urlValueDireccionMicroservice)).build(DireccionService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Direccion addDireccion(Direccion direccion) {
        try {
            return direccionService.addDireccion(direccion);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }


    public Direccion getDireccion(long direccionId) {
        try {
            return direccionService.findDireccionById(direccionId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}
