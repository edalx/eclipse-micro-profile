package com.aiproano.services;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;

/**
 * @author aiproano.
 */
@ApplicationScoped
public class ConsumeConfigApi {
    @RestClient
    private ConfigService configService;
    @Inject
    @ConfigProperty(name = "url-config-server")
    private String urlConfigServer;


    @PostConstruct
    public void init() {
        try {
            System.out.println("URL Config=" + urlConfigServer);
            this.configService = RestClientBuilder.newBuilder().baseUri(new URI(urlConfigServer)).build(ConfigService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key) {
        return configService.configValue(key);
    }
}
