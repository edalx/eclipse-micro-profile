package com.aiproano;

//import com.kumuluz.ee.discovery.annotations.RegisterService;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author aiproano.
 */
@ApplicationPath("/")
@ApplicationScoped
//@RegisterService(value = "persona-service", environment = "dev", version = "1.0.0")
public class PersonaRestApp extends Application {
}
