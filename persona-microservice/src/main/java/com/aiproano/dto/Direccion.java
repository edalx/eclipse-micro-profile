package com.aiproano.dto;

import lombok.Data;

/**
 * @author aiproano.
 */
@Data
public class Direccion {
    private long id;
    private String calle1;
    private String calle2;
    private String numero;

}
