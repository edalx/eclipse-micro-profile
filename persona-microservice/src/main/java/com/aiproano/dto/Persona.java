package com.aiproano.dto;

import lombok.Data;

import javax.json.bind.annotation.JsonbDateFormat;
import java.time.LocalDate;
import java.util.Date;


/**
 * @author aiproano.
 */
@Data
public class Persona {
    private long id;
    private String identificacion;
    private String nombre;
    @JsonbDateFormat("dd-MM-yyyy")
    private LocalDate fechaNacimiento;
    private Direccion direccion;
}
