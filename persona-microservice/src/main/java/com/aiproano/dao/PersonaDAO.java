package com.aiproano.dao;

import com.aiproano.dto.Persona;

import java.util.List;

/**
 * @author aiproano.
 */
public interface PersonaDAO {

    Persona findPersonaById(long idPersona);

    List<Persona> getAllPersona();

    Persona insertPersona(Persona persona);

    Persona updatePersona(Persona persona);

    boolean deletePersona(long id);
}
