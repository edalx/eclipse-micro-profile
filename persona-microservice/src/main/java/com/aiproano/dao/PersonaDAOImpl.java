package com.aiproano.dao;

import com.aiproano.database.ConnectionFactory;
import com.aiproano.dto.Direccion;
import com.aiproano.dto.Persona;
import com.aiproano.services.ConsumeConfigApi;
import com.aiproano.services.ConsumeDireccionApi;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aiproano.
 */
@ApplicationScoped
public class PersonaDAOImpl implements PersonaDAO {
    @Inject
    private ConsumeDireccionApi consumeDireccionApi;
    @Inject
    private ConnectionFactory connectionFactory;
    @Inject
    private ConsumeConfigApi consumeConfigApi;

    private final String POSTGRES_SCHEMA_KEY = "postgres-schema";

    private String postgresSchema;

    @PostConstruct
    public void init() throws SQLException {
        this.postgresSchema = consumeConfigApi.getProperty(POSTGRES_SCHEMA_KEY);
        Connection connection = connectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            stmt.executeQuery(
                    "DROP TABLE " + postgresSchema + "persona;" +
                            "DROP TABLE " + postgresSchema + "direccion;" +
                            "CREATE TABLE " + postgresSchema + "direccion" +
                            "(" +
                            "    id serial PRIMARY KEY," +
                            "    calle_1 VARCHAR(100) NOT NULL," +
                            "    calle_2 varchar(100) NOT NULL," +
                            "    numero varchar(100) NOT NULL" +
                            ");" +
                            "CREATE TABLE " + postgresSchema + "persona" +
                            "(" +
                            "    id serial PRIMARY KEY," +
                            "    identificacion varchar(50) NOT NULL," +
                            "    nombre varchar(50) NOT NULL," +
                            "    fecha_nacimiento date NOT NULL," +
                            "    id_direccion int NOT NULL" +
                            ");"
            );
        } catch (Exception ex) {
            connection.close();
            System.out.println("Error al crear la base de datos");
        }
        connection.close();

    }


    @Override
    public Persona findPersonaById(long idPersona) {
        Connection connection = connectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + postgresSchema + "persona WHERE id=" + idPersona);
            if (rs.next()) {
                connection.close();
                return extractPersonaFromResultSet(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Persona> getAllPersona() {
        try {
            Connection connection = connectionFactory.getConnection();
            List<Persona> personas = new ArrayList<>();

            if (connection != null) {
                System.out.println("Conectado a la base de datos");
            } else {
                System.out.println("Failed to make connection!");
            }

            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + postgresSchema + "persona");
            while (rs.next()) {
                personas.add(extractPersonaFromResultSet(rs));
            }
            connection.close();
            return personas;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Persona insertPersona(Persona persona) {
        try {
            Date date = Date.valueOf(persona.getFechaNacimiento());

            Connection connection = connectionFactory.getConnection();
            Direccion direccion = consumeDireccionApi.addDireccion(persona.getDireccion());
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + postgresSchema + "persona (identificacion, nombre, fecha_nacimiento, id_direccion) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, persona.getIdentificacion());
            ps.setString(2, persona.getNombre());
            ps.setDate(3, date);
            ps.setLong(4, direccion.getId());

            System.out.println(persona.toString());
            int affectedRows = ps.executeUpdate();
            connection.close(); //Cierra la conexion
            if (affectedRows == 0) {
                throw new SQLException("No se ha podido crear la dirección");
            }

            ResultSet generatedKeys = ps.getGeneratedKeys();

            if (generatedKeys.next()) {
                persona.setId(generatedKeys.getLong(1));
                return persona;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public Persona updatePersona(Persona persona) {
        try {
            Date date = Date.valueOf(persona.getFechaNacimiento());
            Connection connection = connectionFactory.getConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE " + postgresSchema + "persona SET identificacion=?, nombre=?, fecha_nacimiento=?, id_direccion=? WHERE id=?");
            ps.setString(1, persona.getIdentificacion());
            ps.setString(2, persona.getNombre());
            ps.setDate(3, date);
            ps.setLong(4, persona.getDireccion().getId());
            ps.setLong(5, persona.getId());

            int affectedRows = ps.executeUpdate();
            connection.close(); //Cierra la conexion

            if (affectedRows == 0) {
                throw new SQLException("No se ha podido actualizar la persona");
            } else {
                return findPersonaById(persona.getId());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deletePersona(long idPersona) {
        try {
            Connection connection = connectionFactory.getConnection();
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM " + postgresSchema + "persona WHERE id=" + idPersona);
            connection.close(); //Cierra la conexion
            if (i == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Persona extractPersonaFromResultSet(ResultSet rs) {
        try {
            Persona user = new Persona();
            Date date = rs.getDate("fecha_nacimiento");
            user.setId(rs.getLong("id"));
            user.setIdentificacion(rs.getString("identificacion"));
            user.setNombre(rs.getString("nombre"));
            user.setFechaNacimiento(date.toLocalDate());
            Direccion direccion = consumeDireccionApi.getDireccion(rs.getLong("id_direccion"));
            user.setDireccion(direccion);
            return user;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
