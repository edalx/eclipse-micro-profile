package com.aiproano.ctrl;

import com.aiproano.dto.Persona;
import com.aiproano.services.ServicioPersonaImpl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.bind.annotation.JsonbDateFormat;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author aiproano.
 */
@Path("/persona")
@RequestScoped
public class PersonaController {
    @Inject
    private ServicioPersonaImpl servicioPersona;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> findAll() {
        return servicioPersona.list();
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Persona addPersona(Persona persona) {
        return servicioPersona.save(persona);
    }

    @POST //Se usa POST dado que @PATCH no es reconocida
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Persona updatePersona(Persona persona) {
        return servicioPersona.update(persona);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Persona findById(@PathParam("id") long id) {
        return servicioPersona.findById(id);
    }

    @DELETE
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean deletePersona(@PathParam("id") long id) {
        return servicioPersona.delete(id);
    }
}
